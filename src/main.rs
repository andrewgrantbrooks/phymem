use std::fs::File;
use std::io;
use std::io::BufReader;
use std::io::prelude::*;
use std::iter::Iterator;
use std::path::Path;

use anyhow::Result;
use clap::{Args, Parser, Subcommand};
use flate2::read::GzDecoder;
use memmap::MmapOptions;
use owo_colors::OwoColorize;

fn main() -> Result<()> {
    let cli = Cli::parse();
    if !cli.quiet {
        check_kernel_cfg();
    }
    cli.run()
}

fn parse_hex<T: From<u64>>(some_str: &str) -> Result<T> {
    let some_str = some_str.strip_prefix("0x").unwrap_or(some_str);
    Ok(u64::from_str_radix(some_str, 16)?.into())
}

/// Check if the running kernel's build config locks down /dev/mem.
/// If so, warn the user; they're not likely to get far.
fn check_kernel_cfg() -> () {
    let filebuf: Option<_> = File::open("/proc/config.gz")
        .map(GzDecoder::new)
        .map(BufReader::new)
        .ok();
    let config = match filebuf {
        None => return,
        Some(bytes) => bytes
    };
    let devmem_locked_down = config
        .lines()
        .take_while(|x| x.is_ok())
        .filter_map(|x| x.ok()).any(|line| &line == "CONFIG_STRICT_DEVMEM=y");
    if devmem_locked_down {
        eprintln!(
            "{} kernel build config has CONFIG_STRICT_DEVMEM=y (mmap() will likely fail)",
            "warning:".bright_magenta().bold()
        );
    }
}

#[derive(Parser)]
#[command(author = "Andrew Brooks")]
#[command(about = "A tool to read/write physical memory")]
#[command(long_about = "A cursèd tool to read/write physical memory (and other things)\n\nThis tool is inherently dangerous; both proper and improper use can fuck up your computer in exciting and surprising ways.")]
#[command(version = "0.1.0")]
struct Cli {
    #[command(subcommand)]
    command: Cmds,

    /// File or device to peek/poke
    #[arg(short, long, default_value = "/dev/mem")]
    devmem: Box<Path>,

    /// Silences warnings
    #[arg(short, long, default_value_t = false)]
    quiet: bool
}

impl Cli {
    pub fn run(&self) -> Result<()> {
        match &self.command {
            Cmds::Poke(op) => op.run(&self.devmem),
            Cmds::Peek(op) => op.run(&self.devmem)
        }
    }
}

#[derive(Subcommand)]
enum Cmds {
    /// Fetches bytes at a location
    #[command(visible_alias = "read")]
    Peek(DoPeek),
    /// DANGEROUS: Sets bytes at a location
    #[command(visible_alias = "write")]
    Poke(DoPoke),
}

#[derive(Args)]
struct DoPeek {
    /// Hexadecimal address to read out of
    #[arg(value_parser = parse_hex::<u64>)]
    address: u64,

    /// Hexadecimal number of bytes to read
    #[arg(value_parser = parse_hex::<u64>, default_value_t = 8)]
    length: u64,

    /// Where to write output
    #[arg(long, short, default_value = "/dev/stdout")]
    output: Box<Path>
}
impl DoPeek {
    /// Execute the peek command
    pub fn run(&self, devmem: &Path) -> Result<()> {
        let length = self.length as usize;
        let mmap = unsafe {
            MmapOptions::new()
                .offset(self.address)
                .len(length)
                .map(&File::open(devmem)?)?
        };
        let mut output =
            File::options()
              .write(true)
              .open(&self.output)?;
        output.write_all(&mmap[0..length])?;
        Ok(())
    }
}

#[derive(Args)]
struct DoPoke {
    /// Hexadecimal address to write into
    #[arg(value_parser = parse_hex::<u64>)]
    address: u64,

    /// # bytes to write
    /// When unspecified, defaults to length of input.
    #[arg(long, short, value_parser = parse_hex::<u64>)]
    length: Option<u64>,

    /// File to read written bytes from
    #[arg(long, short, default_value = "/dev/stdin")]
    input: Box<Path>
}
impl DoPoke {
    /// Execute the poke command
    pub fn run(&self, devmem: &Path) -> Result<()> {
        // read entirely into memory before poking
        let bytes_to_poke = self.stage_poke_bytes()?;
        let length = bytes_to_poke.len();
        let mut mmap = unsafe {
            MmapOptions::new()
                .offset(self.address)
                .len(length)
                .map_mut(&File::open(devmem)?)?
        };
        mmap.copy_from_slice(&bytes_to_poke);
        Ok(mmap.flush()?)
    }

    /// Load all input bytes to poke onto the heap.
    fn stage_poke_bytes(&self) -> Result<Vec<u8>> {
        // TODO: are partial reads handled...?
        let mut input = File::open(&self.input)?;
        let mut bytes = vec![];
        match self.length {
            Some(l) => io::copy(&mut input.take(l), &mut bytes),
            _       => io::copy(&mut input,         &mut bytes)
        }?;
        Ok(bytes)
    }
}

#[cfg(test)]
mod tests {
    use crate::parse_hex;
    #[test]
    fn parse_hex_to_option() {
        assert_eq!(parse_hex::<Option<u64>>("0x000D5C10").unwrap(), Some(875536));
        assert_eq!(parse_hex::<Option<u64>>("0xD5C10").unwrap(), Some(875536));
        assert_eq!(parse_hex::<Option<u64>>("D5C10").unwrap(), Some(875536));
        assert_eq!(parse_hex::<Option<u64>>("d5c10").unwrap(), Some(875536));
    }

    #[test]
    fn parse_hex_to_u64() {
        assert_eq!(parse_hex::<u64>("0x000D5C10").unwrap(), 875536);
        assert_eq!(parse_hex::<u64>("0xD5C10").unwrap(), 875536);
        assert_eq!(parse_hex::<u64>("D5C10").unwrap(), 875536);
        assert_eq!(parse_hex::<u64>("d5c10").unwrap(), 875536);
    }

}
