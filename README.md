# `phymem`: peek/poke physical memory on Linux

This is a personal tool intended to aid ACPI debugging/experimentation.

⚠️ **Because this tool is used to read/write physical memory, it is inherently dangerous.** ⚠️ 

It can cause system instability and damage when used improperly (and maybe properly, too). If you don't understand why, you probably shouldn't use it. You _definitely_ shouldn't play with the `poke`/`write` subcommands.

# Building

## With Nix

### Without checking out this repo

`nix run gitlab:andrewgrantbrooks/phymem` will download, build, and run this program. You'll need to specify other arguments (the built-in help will tell you more).

### With a copy of this repo

Run `nix build`.

The resulting executable will be appear under `result/bin/phymem`. It should be byte-for-byte identical to what I get when I build it.

This project does use Nix's "flakes" feature, which you may need to enable if you haven't already.

The usual Nix flake commands (`nix shell`, `nix develop`, etc.) should work just fine.

## Without Nix

We don't recommend this b/c it's less reliable (and untested). Anyway, since there's no accounting for taste:

1. Get a Rust toolchain however you're comfortable doing so (why not try `rustup`?).

2. Run `cargo build --release`.

3. Hope nothing breaks due to a quirk of your distribution, environment, or Rust toolchain version.

# Usage

Refer to the built-in help (`devmem --help`).

# Kernel configuration

Most Linux distributions' kernels lock down `/dev/mem` to prevent physical memory access. This is usually a reasonable security measure. There are extremely few good reasons to directly manipulate physical memory from userspace.

While we may not question your justification for using this program, your distribution's kernel maintainer might. Therefore, it may be necessary to recompile with a custom kernel that sets `CONFIG_STRICT_DEVMEM=n` in the kernel configuration.

`phymem` will attempt to detect incompatible kernels and will display a warning to `stderr` if it's suspicious of your kernel. Use the `-q` flag to shut it up if it's mistaken.

## Patching your kernel on NixOS

In your `configuration.nix`, add something this to your `boot.kernelPatches` list:

```nix
{
  name = "user-phymem-access";
  patch = null;
  extraStructuredConfig = with pkgs.lib; {
    STRICT_DEVMEM    = kernel.no;
    IO_STRICT_DEVMEM = mkForce kernel.unset;
  };
}
```

Afterward, `nixos-rebuild` as usual and reboot into the new kernel.

_Don't forget to remove this patch when you're done._ 

## Patching your kernel on other Linux distros

This varies a lot by distribution, so consult its docs. We're sorry, godspeed, and good luck.
