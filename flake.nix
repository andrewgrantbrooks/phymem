{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
      LIBCLANG_PATH = "${pkgs.llvmPackages_latest.libclang}/lib"; # formerly llvm 14, specifically
    in {
      packages.default = with pkgs; rustPlatform.buildRustPackage {
        name = "phymem";
        src = ./.;
        inherit LIBCLANG_PATH;
        cargoLock = {
          lockFile = ./Cargo.lock;
        };
        meta = {
          broken = !pkgs.stdenv.isLinux;
          description = "read/write physical memory at whim";
        };
        passthru.exePath = "/bin/phymem";
      };

      devShell = with pkgs; mkShell {
        inputsFrom = [self.packages."${system}".default];
        nativeBuildInputs = [
          clippy
          rust-analyzer
          rustfmt
        ];
        RUST_SRC_PATH = rustPlatform.rustLibSrc;
        RUST_BACKTRACE = "1";
        inherit LIBCLANG_PATH;
      };

      # Enables 'nix run'.
      apps.default = flake-utils.lib.mkApp {
        drv = self.packages.${system}.default;
      };
    }
  );
}
